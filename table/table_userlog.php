<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_userlog extends discuz_table {
	
	public function __construct() {
		$this->_table = 'userlog';
		$this->_pk = 'id';
	}
	
	public function update_logout($uid, $logout) {
		DB::query('UPDATE %t SET logout=%d WHERE uid=%d ORDER BY id DESC LIMIT 1', array($this->_table, $logout, $uid));
	}
	
	public function fetch_lastlog_by_uid($uid) {
		$logout = DB::result_first('SELECT logout FROM %t WHERE uid=%d ORDER BY id DESC LIMIT 1', array($this->_table, $uid));
		return $logout;
	}
	
	public function count_by_search($condition) {
		return DB::result_first("SELECT COUNT(*) FROM %t WHERE 1 %i", array($this->_table, $condition));
	}
	
	public function fetch_all_by_search($condition, $start, $ppp) {
		return DB::fetch_all("SELECT * FROM %t WHERE 1 %i ORDER BY id DESC LIMIT %d, %d", array($this->_table, $condition, $start, $ppp));
	}
}