<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$sql = <<<EOF

DROP TABLE IF EXISTS pre_userlog;
CREATE TABLE `pre_userlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) NOT NULL,
  `username` char(15) NOT NULL,
  `ip` char(15) NOT NULL,
  `port` int(5) NOT NULL,
  `login` int(10) NOT NULL,
  `logout` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) TYPE=MyISAM;

EOF;

runquery($sql);

$finish = TRUE;

?>