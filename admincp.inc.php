<?php

$lang = $scriptlang['dzsup_userlog'];

$ppp = 100;
$srchadd = $searchtext = $extra = $srchuid = '';
$page = max(1, intval($_GET['page']));

if(!empty($_GET['srchuid'])) {
	$srchuid = intval($_GET['srchuid']);
	$srchadd = "AND uid='$srchuid'";
} elseif(!empty($_GET['srchusername'])) { 
	$srchuid = C::t('common_member')->fetch_uid_by_username($_GET['srchusername']);
	if($srchuid) {
		$srchadd = "AND uid='$srchuid'";
	}
}

if($srchuid) {
	$extra = '&srchuid='.$srchuid;
}

showtableheader();
showformheader('plugins&operation=config&do='.$pluginid.'&identifier=dzsup_userlog&pmod=admincp', 'userlogsubmit');
showsubmit('userlogsubmit', $lang['search'], $lang['username'].': <input name="srchusername" value="'.htmlspecialchars($_GET['srchusername']).'" class="txt" />&nbsp;&nbsp;', $searchtext);
showformfooter();

echo '<tr class="header"><th>'.$lang['username'].'</th><th>'.$lang['ip'].'</th><th>'.$lang['port'].'</th><th>'.$lang['login'].'</th><th>'.$lang['logout'].'</th><th></th></tr>';
if(submitcheck('userlogsubmit')) {
	$count = C::t('#dzsup_userlog#userlog')->count_by_search($srchadd);
	$userlog = C::t('#dzsup_userlog#userlog')->fetch_all_by_search($srchadd, ($page - 1) * $ppp, $ppp);
	foreach($userlog as $log) {
		$log['login'] = dgmdate($log['login'], 'Y-m-d H:i:s');
		$log['logout'] = dgmdate($log['logout'], 'Y-m-d H:i:s');
		echo '<tr><td>'.$log['username'].'</td><td>'.$log['ip'].'</td><td>'.$log['port'].'</td><td>'.$log['login'].'</td><td>'.$log['logout'].'</td></tr>';
	}
}

showtablefooter();

echo multi($count, $ppp, $page, ADMINSCRIPT."?action=plugins&operation=config&do=$pluginid&identifier=myrepeats&pmod=admincp$extra");