<?php

class plugin_dzsup_userlog {
	function common(){
		global $_G;
		if($_G['uid']) {
			$oltimespan = $_G['cache']['plugin']['dzsup_userlog']['timespan'];
			//dsetcookie('userlog_active', authcode(TIMESTAMP, 'ENCODE'), 31536000);
			//$ulastactivity = authcode($_G['cookie']['ulastactivity'], 'DECODE'); 
			//$uactive = authcode(getcookie('userlog_active'), 'DECODE');
			//if(empty($uactive)) {
			//	$uactive = getuserprofile('lastactivity');
			//	dsetcookie('userlog_active', authcode($uactive, 'ENCODE'), 31536000);
			//}
			
			$logout = C::t('#dzsup_userlog#userlog')->fetch_lastlog_by_uid($_G['uid']);
			if($_G['uid'] && TIMESTAMP - $logout > $oltimespan * 60 || !$logout) {
				C::t('#dzsup_userlog#userlog')->insert(array(
					'uid' => $_G['uid'],
					'username' => $_G['username'],
					'ip' => $_G['clientip'],
					'port' => $_SERVER['REMOTE_PORT'],
					'login' => TIMESTAMP,
					'logout' => TIMESTAMP,
				));
			} else {
				C::t('#dzsup_userlog#userlog')->update_logout($_G['uid'], TIMESTAMP);
				//dsetcookie('userlog_active', authcode(TIMESTAMP, 'ENCODE'), 31536000);
			}
		}
	}
}